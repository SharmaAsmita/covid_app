import axios from "axios";

export default {
    namespaced: true,
    state: {
      states: [],
    },
    mutations: {
      setState(state, payload) {
          state.states = payload;
        },
    },


    actions: {
        async getCovidAPI({ commit }){
          alert("home.js");
          try {
            let res = await axios.get("https://api.covidtracking.com/v1/states/current.json")
            console.log(res)
            commit('setState', res.data)
          }
          catch (e) {
            console.error("Error>>>>", e);
          }
        },
    }
}
